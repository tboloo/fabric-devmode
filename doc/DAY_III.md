# DAY III
So basic example from day II works, however there is a significant issue with it, namely there is no way to interact with the blockchain since there is no user who could do so.

In order to have some interaction with the ledger we (like running the [Hyperledger Explorer](https://github.com/hyperledger/blockchain-explorer}) we need to set up full blown network, consisting of:
1. Peer node
2. Orderer node
3. Certificate authority (ca)
4. Command line tools container 
5. Chaincode container, used to build and run chaincode (optional, one could have it as a local development environment)

the docker-compose file used is based on the file from (basic-network)[https://github.com/hyperledger/fabric-samples/tree/master/basic-network] sample, although with some tweaks:
```diff
diff --git a/docker-compose.yml b/docker-compose.yml
index b582a2c..a9f4833 100644
--- a/docker-compose.yml
+++ b/docker-compose.yml
@@ -15,7 +15,7 @@ services:
       - FABRIC_CA_HOME=/etc/hyperledger/fabric-ca-server
       - FABRIC_CA_SERVER_CA_NAME=ca.example.com
       - FABRIC_CA_SERVER_CA_CERTFILE=/etc/hyperledger/fabric-ca-server-config/ca.org1.example.com-cert.pem
-      - FABRIC_CA_SERVER_CA_KEYFILE=/etc/hyperledger/fabric-ca-server-config/4239aa0dcd76daeeb8ba0cda701851d14504d31aad1b2ddddbac6a57365e497c_sk
+      - FABRIC_CA_SERVER_CA_KEYFILE=/etc/hyperledger/fabric-ca-server-config/689329f7c94d77a1e9b3581a509c108f2160aa964b62dd2f1c9c1fdda77bc258_sk
     ports:
       - "7054:7054"
     command: sh -c 'fabric-ca-server start -b admin:adminpw'
@@ -29,18 +29,19 @@ services:
     container_name: orderer.example.com
     image: hyperledger/fabric-orderer:$TAG
     environment:
-      - ORDERER_GENERAL_LOGLEVEL=info
+      - ORDERER_GENERAL_LOGLEVEL=debug
       - ORDERER_GENERAL_LISTENADDRESS=0.0.0.0
       - ORDERER_GENERAL_GENESISMETHOD=file
       - ORDERER_GENERAL_GENESISFILE=/etc/hyperledger/configtx/genesis.block
       - ORDERER_GENERAL_LOCALMSPID=OrdererMSP
       - ORDERER_GENERAL_LOCALMSPDIR=/etc/hyperledger/msp/orderer/msp
+      - ORDERER_GENERAL_GENESISPROFILE=SampleSingleMSPSolo
     working_dir: /opt/gopath/src/github.com/hyperledger/fabric/orderer
     command: orderer
     ports:
       - 7050:7050
     volumes:
-        - ./config/:/etc/hyperledger/configtx
+        - ./artifacts/:/etc/hyperledger/configtx
         - ./config/crypto-config/ordererOrganizations/example.com/orderers/orderer.example.com/:/etc/hyperledger/msp/orderer
         - ./config/crypto-config/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/:/etc/hyperledger/msp/peerOrg1
     networks:
@@ -52,8 +53,8 @@ services:
     environment:
       - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
       - CORE_PEER_ID=peer0.org1.example.com
-      - CORE_LOGGING_PEER=info
-      - CORE_CHAINCODE_LOGGING_LEVEL=info
+      - CORE_LOGGING_PEER=debug
+      - CORE_CHAINCODE_LOGGING_LEVEL=debug
       - CORE_PEER_LOCALMSPID=Org1MSP
       - CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/peer/
       - CORE_PEER_ADDRESS=peer0.org1.example.com:7051
@@ -61,8 +62,8 @@ services:
       # # bridge network as the peers
       # # https://docs.docker.com/compose/networking/
       - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=${COMPOSE_PROJECT_NAME}_basic
-      - CORE_LEDGER_STATE_STATEDATABASE=CouchDB
-      - CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS=couchdb:5984
+      - CORE_LEDGER_STATE_STATEDATABASE=LevelDB
+      # - CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS=couchdb:5984
       # The CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME and CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD
       # provide the credentials for ledger to connect to CouchDB.  The username and password must
       # match the username and password set for the associated CouchDB.
@@ -78,7 +79,7 @@ services:
         - /var/run/:/host/var/run/
         - ./config/crypto-config/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/msp:/etc/hyperledger/msp/peer
         - ./config/crypto-config/peerOrganizations/org1.example.com/users:/etc/hyperledger/msp/users
-        - ./config:/etc/hyperledger/configtx
+        - ./artifacts:/etc/hyperledger/configtx
     depends_on:
       - orderer.example.com
       # - couchdb
@@ -105,7 +106,7 @@ services:
     environment:
       - GOPATH=/opt/gopath
       - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
-      - CORE_LOGGING_LEVEL=info
+      - CORE_LOGGING_LEVEL=debug
       - CORE_PEER_ID=cli
       - CORE_PEER_ADDRESS=peer0.org1.example.com:7051
       - CORE_PEER_LOCALMSPID=Org1MSP
@@ -115,7 +116,7 @@ services:
     command: /bin/bash
     volumes:
         - /var/run/:/host/var/run/
-        - ./chaincode/:/opt/gopath/src/chaincodedev/chaincode
+        - ./chaincode/:/opt/gopath/src/github.com/
         - ./config/crypto-config:/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/
     networks:
         - basic
@@ -131,10 +132,11 @@ services:
     environment:
       - GOPATH=/opt/gopath
       - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
-      - CORE_LOGGING_LEVEL=DEBUG
-      - CORE_PEER_ID=example02
+      - CORE_LOGGING_LEVEL=debug
+      - CORE_PEER_ID=chaincode
       - CORE_PEER_ADDRESS=peer0.org1.example.com:7051
-      - CORE_PEER_LOCALMSPID=DEFAULT
+      - CORE_PEER_LOCALMSPID=Org1MSP
+      - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=${COMPOSE_PROJECT_NAME}_basic
       - CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp
     working_dir: /opt/gopath/src/chaincode
     command: /bin/bash -c 'sleep 6000000'
@@ -142,5 +144,7 @@ services:
         - /var/run/:/host/var/run/
         - ./config/crypto-config/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/msp:/etc/hyperledger/msp
         - ./chaincode:/opt/gopath/src/chaincode
+    networks:
+      - basic
     depends_on:
       - cli
```
## Setup

Our setup is based on [Basic network](https://github.com/hyperledger/fabric-samples/tree/master/basic-network) example.
We will start with defining the necessary services and folder structure. Our solution looks now as follows:
```bash
├── bin (hyperledger excutables)
│   ├── configtxgen
│   ├── configtxlator
│   └── cryptogen
├── chaincode
│   └── chaincode_example02.go
├── CHANGELOG
├── config (description of the organisation, also configuration files for cryptographic materials)
│   ├── configtx.yaml
│   └── crypto-config.yaml
├── doc (this guide)
│   ├── DAY_III.md
│   ├── DAY_II.md
│   └── DAY_I.md
├── docker-compose.yml
├── LICENSE
├── README.md
└── scripts (scripts to run the cryptographic material generation, spin off docker, etc.)
```

Let's start with generation of required crypto materials. First it's necessary to set up some environment variables:

```bash
$ export PATH=${PWD}/bin:$PATH
$ export FABRIC_CFG_PATH=${PWD}/config
```

The latter, `FABRIC_CFG_PATH` tells `configtxgen` where to look for configuration files.

Now we need to generate necessary crypto material

```bash
$ ./scripts/generate.sh
```

Remember to update the `FABRIC_CA_SERVER_CA_KEYFILE` setting in [docker-compose.yml](docker-compose.yml) (this long alphanumeric string, which will change upon eah generation)

After that, we are ready to spin off docker 

```bash
$ docker-compose up -d
```

When all the containers are running, we need to create and join the channel. This is done by the invoking another script, channel_setup.sh

```bash
$ ./scripts/channel_setup.sh
```

Now onto the chaincode run:

```bash
$ docker exec -it chaincode bash
root@46b958f638c0:/opt/gopath/src/chaincode/ex02#
CORE_PEER_ADDRESS=peer0.org1.example.com:7052 CORE_CHAINCODE_ID_NAME=mycc:0 ./chaincode_example02 #Notice 7052 port, I guess it's the expected port when run in devmode
```

Now it's time to install and instantiate the chaincode

```bash
$ docker exec -it cli bash
root@a2107a44eef3:/opt/gopath/src/github.com#
root@a2107a44eef3:/opt/gopath/src/github.com# peer chaincode install -p github.com/ex02/ -n mycc -v 0
root@a2107a44eef3:/opt/gopath/src/github.com# peer chaincode instantiate -n mycc -v 0 -c '{"Args":["init","a","100","b","200"]}' -C mychannel
root@a2107a44eef3:/opt/gopath/src/github.com# peer chaincode invoke -n mycc -c '{"Args":["invoke","a","b","10"]}' -C mychannel
root@a2107a44eef3:/opt/gopath/src/github.com# peer chaincode query -n mycc -c '{"Args":["query","a"]}' -C mychannel
```

Last command should yield '90'

It's crucial to follow the go packages naming - if you mess up package name while installing chaincode you won't be able to instantiate it with - it will fail with some obscure access errors.