# DAY X

## Working with identities.

We have worked with identities previously, using [fabric-ca-cli](https://hyperledger-fabric-ca.readthedocs.io/en/latest/clientcli.html)
This time we will use API to achieve the same goal, using [IdentityService](https://fabric-sdk-node.github.io/IdentityService.html)
Implementation details are in [app/getAllIdentities.js](app/getAllIdentities.js)

To update user we need to use [IdentityRequest](https://fabric-sdk-node.github.io/global.html#IdentityRequest)

```javascript
var request = {enrollmentID:user,affiliation:'org1',attrs:[]}
attr1 = {name:attribute,value:value,ecert:true}
request.attrs.push(attr1)
let response = await identityService.update(user,request,adminFromStore);
```

