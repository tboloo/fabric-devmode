# DAY VI

Today we will switch from [Sublime Text](https://www.sublimetext.com/3) to [GoLand](https://www.jetbrains.com/go/) - fully fledged Go IDE to develop our code.

Let's start with project structure. As per [official documentation](https://golang.org/doc/code.html#Organization) we should organize code and libraries into workspace, containig src/ pkg/ and bin/ subfolders.

The GOPATH path will be pointing to [chaincode/](chaincode/) folder, GOROOT to go installation directory, and also we would update PATH environemnt variable.

![GOROOT](doc/img/GOROOT.png "GOROOT")

![GOPATH](doc/img/GOPATH.png "GOPATH")

![PATH](doc/img/PATH.png "PATH")

Since there is no gcc on Windows platform, at least not by default, we would have to add `-tags nopkcs11` tag to all go commands (build, install, get).

Let's start with building the code:

```bash
C:\Users\(...)\Documents\Development\go\fabric-devmode\chaincode>go build -tags nopkcs11 github.com\tboloo\acl
```

Package name does not include `src\`

Side note: In order to set logging level, set `CORE_CHAINCODE_LOGGING_LEVEL` environment variable. Such variables could be set in Run/Debug configuration, like so:

![ENVIRONMENT](doc/img/ENVIRONMENT.png "ENVIRONMENT")
