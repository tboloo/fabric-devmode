# DAY IX
This time we will concentrate on further improvements of the acl chaincode, and on upgrading chaincode without restarting environment.

```diff
diff --git a/chaincode/src/github.com/tboloo/acl/acl.go b/chaincode/src/github.com/tboloo/acl/acl.go
index 961528e..38acc0b 100644
--- a/chaincode/src/github.com/tboloo/acl/acl.go
+++ b/chaincode/src/github.com/tboloo/acl/acl.go
@@ -31,6 +31,9 @@ func (t *SimpleChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
                // Make payment of X units from A to B
                return t.user_info(stub, args)
        }
+       if function == "get_attribute" {
+               return t.get_attribute(stub, args)
+       }
        return shim.Error("Invalid invoke function name. Expecting \"user_info\".")
 }

@@ -56,6 +59,22 @@ func (t* SimpleChaincode) user_info(stub shim.ChaincodeStubInterface, args []str
        return shim.Success([]byte(jsonResp))
 }

+func (t* SimpleChaincode) get_attribute(stub shim.ChaincodeStubInterface, args []string) pb.Response {
+       var attribute_name string
+       if len(args) != 1 {
+               return shim.Error("Incorrect number of arguments. Expecting only attribute name")
+       }
+       attribute_name = args[0]
+       val, ok, err := cid.GetAttributeValue(stub, attribute_name)
+       if err != nil {
+               return shim.Error("Error retriving attribute " + attribute_name + ", error: " + err.Error())
+       }
+       if !ok {
+               return shim.Error("User does not have attribute " + attribute_name)
+       }
+       return shim.Success([]byte(val))
```

We have added function to retrive single attribute value from user.

Attributes can be added during registration, like in [registerUser.js](app/registerUser.js), or using the fabric-ca-client tool.
```
root@2768af30a8e2:/# fabric-ca-client identity modify john.doe --attrs attr2=host:ecert
```
above command can be executed by enrolled user, so remember to run following script first
```
root@2768af30a8e2:/# export FABRIC_CA_CLIENT_HOME=/etc/hyperledger/fabric-ca; fabric-ca-client enroll -u http://admin:adminpw@ca.org1.example.com:7054
```

To upgrade running chaincode we need to do as follows:
1. Build and run new version of the chaincode: `./scripts/build_and_run_chaincode.sh -p github.com/tboloo/acl -n acl -v 1`
2. Install new chaincode `./scripts/install_and_instantiate_chaincode.sh -p github.com/tboloo/acl -n acl -v 1 -c mychannel` (ignore the error)
3. Finally upgrade chaincode `./scripts/upgrade_chaincode.sh -n acl -v 2 -c mychannel` (one can specify initial arguments using -i flag)

## Updating user 
```bash
docker exec -it ca.org1.example.com bash
root@2768af30a8e2:/# fabric-ca-client enroll -u http://admin:adminpw@ca.org1.example.com:7054
root@2768af30a8e2:/# fabric-ca-client identity list
Name: new.user, Type: client, Affiliation: org1, Max Enrollments: -1, Attributes: [{Name:attr1 Value:fencer ECert:true} {Name:hf.EnrollmentID Value:new.user ECert:true} {Name:hf.Type Value:client ECert:true} {Name:hf.Affiliation Value:org1 ECert:true} {Name:attribute1 Value:value1 ECert:true} {Name:attribute2 Value:value2 ECert:true}]
```

```bash
root@2768af30a8e2:/# fabric-ca-client identity modify new.user --secret=q1w2e3r4 --maxenrollments=-1
2018/06/06 07:12:48 [INFO] Configuration file location: /etc/hyperledger/fabric-ca-server/fabric-ca-client-config.yaml
Successfully modified identity - Name: new.user, Type: client, Affiliation: org1, Max Enrollments: -1, Secret: q1w2e3r4, Attributes: [{Name:hf.Type Value:client ECert:true} {Name:hf.Affiliation Value:org1 ECert:true} {Name:attribute1 Value:value1 ECert:true} {Name:attribute2 Value:value2 ECert:true} {Name:attr1 Value:fencer ECert:true} {Name:hf.EnrollmentID Value:new.user ECert:true}]
```

```bash
root@2768af30a8e2:/# fabric-ca-client identity modify new.user --attrs 'attribute3=value3:ecert'
2018/06/06 07:18:02 [INFO] Configuration file location: /etc/hyperledger/fabric-ca-server/fabric-ca-client-config.yaml
Successfully modified identity - Name: new.user, Type: client, Affiliation: org1, Max Enrollments: -1, Secret: , Attributes: [{Name:attribute1 Value:value1 ECert:true} {Name:attribute2 Value:value2 ECert:true} {Name:attribute3 Value:value3 ECert:true} {Name:attr1 Value:fencer ECert:true} {Name:hf.EnrollmentID Value:new.user ECert:true} {Name:hf.Type Value:client ECert:true} {Name:hf.Affiliation Value:org1 ECert:true}]
```

Now the user has to be updated locally - enrollment information needs to be refreshed. We  will do it using [updateUserEnrollment.js](app/updateUserEnrollment.js)
```
root@1b4229f9aa1d:/var/app# node user.js new.user q1w2e3r4
```

Now we can query for updated attribute using [getAttribute.js](app/getAttribute.js): 

``` 
root@1b4229f9aa1d:/var/app# node getAttribute.js new.user attribute5
```
