# DAY VIII

This time we will spend some time on refactoring and cleaning up our solution.
Let's start with scripts. In order to make them more generic, we will refactor them to accept parameters.
Since bash does not have optparse, we will use following solution:

```bash
#!/bin/bash
set -ev

usage() {
    echo "Usage: $0
    [-p|--package <package name>]
    [-n|--name <chaincode name>]
    [-v|--version <chaincode version> " 1>&2;
    exit 0;
}

PACKAGE="chaincode/pkg"
BINARY=chaincode
CHAINCODE_NAME=chaincode
CHAINCODE_VERSION=0

while (( "$#" )); do
  case "$1" in
    -p | --package)
      PACKAGE=$2
      shift 2
      ;;
    -n | --name)
      CHAINCODE_NAME=$2
      shift 2
      ;;
    -v | --version)
      CHAINCODE_VERSION=$2
      shift 2
      ;;
    -h | --help )
      usage
      break;;
  esac
done
# set positional arguments in their proper place
eval set -- "$PARAMS"

echo "getting govendor and go-logging"
docker exec --interactive --tty --workdir=/opt/gopath/src/${PACKAGE} chaincode go get -u github.com/kardianos/govendor
docker exec --interactive --tty --workdir=/opt/gopath/src/${PACKAGE} chaincode go get -u github.com/op/go-logging
echo "initializing dependencies"
docker exec --interactive --tty --workdir=/opt/gopath/src/${PACKAGE} chaincode govendor init
echo "fetching dependencies"
docker exec --interactive --tty --workdir=/opt/gopath/src/${PACKAGE} chaincode govendor fetch github.com/hyperledger/fabric/core/chaincode/lib/cid
echo "building chaincode"
docker exec --interactive --tty --workdir=/opt/gopath/src/${PACKAGE} chaincode go build -o ${BINARY}
echo "starting chaincode"
docker exec --interactive --tty --workdir=/opt/gopath/src/${PACKAGE} chaincode ${CORE_CHAINCODE_ID}:${CHAINCODE_VERSION} ./${BINARY}
```

We also clean up workspace, so that it follow go project structure:
```bash
.
├── app
│   ├── app.js
│   ├── enrollAdmin.js
│   ├── hfc-key-store
│   ├── node_modules
│   ├── package.json
│   ├── package-lock.json
│   ├── query.js
│   ├── registerUser.js
│   └── users
├── artifacts
│   ├── channel.tx
│   ├── genesis.block
│   └── Org1MSPanchors.tx
├── bin
│   ├── configtxgen
│   ├── configtxlator
│   └── cryptogen
├── chaincode
│   ├── bin
│   ├── pkg
│   └── src
├── config
│   ├── config.json
│   ├── configtx.yaml
│   ├── crypto-config
│   └── crypto-config.yaml
├── doc
│   ├── DAY_III.md
│   ├── DAY_II.md
│   ├── DAY_I.md
│   ├── DAY_IV.md
│   ├── DAY_VIII.md
│   ├── DAY_VII.md
│   ├── DAY_VI.md
│   ├── DAY_V.md
│   └── img
├── docker-compose.yml
├── LICENSE
├── README.md
└── scripts
    ├── build_and_run_chaincode.sh
    ├── generate_crypto_material.sh
    ├── install_and_instantiate_chaincode.sh
    └── setup_channel.sh
```

Now we have working code. Important note: only attributes embedded in ecert are retrived by `cid.GetAttributeValue`, so when registering user we should use `ecert:true`, like in following snippet:

```javascript
fabric_ca_client.register({enrollmentID: 'john.does', affiliation: 'org1',role: 'client',attrs: [{"name":"attr1","value":"fencer","ecert":true}]}, admin_user);
```