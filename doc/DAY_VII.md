# DAY VII

This time we are finally going to create real application interacting with Fabric network. 
We will use [node-sdk](https://github.com/hyperledger/fabric-sdk-node).
Alse, we will be working with users, that is real identities interacting with ledger.

First little updates to use_chaincode.
```diff
diff --git a/chaincode/usr/usr_chaincode.go b/chaincode/usr/usr_chaincode.go
index b3bb873..6388186 100644
--- a/chaincode/usr/usr_chaincode.go
+++ b/chaincode/usr/usr_chaincode.go
@@ -71,7 +71,7 @@ func (t *SimpleChaincode) user_info(stub shim.ChaincodeStubInterface, args []str
        // Do something with the value of 'val'
        jsonResp := "{\"id\":\"" + id + "\",\"mspid\":\"" + mspid + "\"" + "\",\"attributes\":\"" + val + "\"}"
        logger.Debug(jsonResp)
-       return shim.Success(nil)
+       return shim.Success([]byte(jsonResp))
 }
```

and also two new scripts, [build_chaincode.sh](scripts/build_chaincode.sh) and [install_instantiate_chaincode.sh](scripts/install_instantiate_chaincode.sh) to automate chaincode building and deployment.

Updated procedure:
```bash
$ ./scripts/build_chaincode.sh
$ docker exec -it chaincode bash
root@0202ce9f14a1:/opt/gopath/src/chaincode/usr# CORE_CHAINCODE_ID_NAME=usr:0 ./usr_chaincode 
```

And in second terminal
```bash
$ ./scripts/install_instantiate_chaincode.sh
```

So, let's get to the code.
Out solution will be developed using [Hyperledger Fabric Client SDK for Node.js](https://github.com/hyperledger/fabric-sdk-node)
First, we need runtime environment. Let's update our [docker-compose.yml](docker-compose.yml) file first.

```diff
diff --git a/docker-compose.yml b/docker-compose.yml
index 84f5410..fb7c8e9 100644
--- a/docker-compose.yml
+++ b/docker-compose.yml

+
+  web:
+    container_name: web
+    image: node:9-slim
+    tty: true
+    environment:
+      - LOG_LEVEL=debug
+    working_dir: /var/app
+    volumes:
+      - ./app:/var/app
+      - ./config/crypto-config/peerOrganizations/org1.example.com/ca/:/var/app/hfc-key-store
+    command:
+      - /bin/bash
+    ports:
+      - 4000:4000
+    depends_on:
+      - orderer.example.com
+    networks:
+      - basic
+
```

Important things: we would use private key for peer1 CA, so we mount aproporiate volume.
Now the code. First, application requirements [package.json](package.json).
Next, code to enroll admin, [enrollAdmin.js](enrollAdmin.js), register first user, [registerUser.js](registerUser.js) and query the ledger, [query.js](query.js). Code is mostly copied from [Hyperledger Fabric Sample Application](https://github.com/hyperledger/education/tree/master/LFS171x/fabric-material/tuna-app), which is a part of an EDX [Blockchain for Business - An Introduction to Hyperledger Technologies](https://courses.edx.org/courses/course-v1:LinuxFoundationX+LFS171x+3T2017/course/) course.

So first we need to login to web container, and install required packages:
```bash
$ docker exec -it web bash
root@3cc71c69c625:/var/app# apt-get update && apt-get install python build-essential
root@3cc71c69c625:/var/app# npm install
root@3cc71c69c625:/var/app# node enrollAdmin.js
```
...and this won't work... It turns out that the name of the CA on the Connection Profile must be the name of the server, as a global defined by
- FABRIC_CA_SERVER_CA_NAME= $CA_NAME So the $CA_NAME must be set in your Connection Profile, and not the containers name. Our FABRIC_CA_SERVER_CA_NAME is `ca.example.com`, not `ca.org1.example.com`, do the Fabric CA Client should be constructed like so:
```javascript
    fabric_ca_client = new Fabric_CA_Client('http://ca.org1.example.com:7054', tlsOptions , 'ca.example.com', crypto_suite);
```

or, our [docker-compose.yml](docker-compose.yml) should be updated.
Also, proper naming is essential while interacting with fabric-ca-client. Mistake in CA address results in some strange errors, like 
```bash
fabric-ca-client Error Code: 20 - Authorization failure
```

Anyway, after fixing above we can enjoy interaction with the Fabric network.