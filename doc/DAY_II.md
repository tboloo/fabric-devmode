# DAY II
Now it's time to run sample chaincode. We will base on chaincode_example02 from the fabric-samples repository.
Download the file, put it in the chaincode subdirectory and add logging package, like so (lines to be added in bold):

```go
import (
        ...
// Omitted for brevity
    "github.com/op/go-logging"
)

var logger = logging.MustGetLogger("ex02")

// SimpleChaincode example simple Chaincode implementation
type SimpleChaincode struct {
}

func (t *SimpleChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response {
    logger.Debug("Init Chaincode...")
    fmt.Println("ex02 Init")
    _, args := stub.GetFunctionAndParameters()
    var A, B string // Entities
    var Aval, Bval int // Asset holdings
        ...
// Omitted for brevity
}

func (t *SimpleChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
    fmt.Println("ex02 Invoke")
    logger.Debug("Invoking Chaincode...")
    function, args := stub.GetFunctionAndParameters()
        ...
// Omitted for brevity
```

Now it's time to build the chaincode. Go to the chaincode container, 

```bash
docker exec -it chaincode bash
```

install logging package in the container

```bash
root@c3a30b6a5dfd:/opt/gopath/src/chaincode# go get github.com/op/go-logging
```

and build chaincode

```bash
root@c3a30b6a5dfd:/opt/gopath/src/chaincode# go build -o chaincode_example02
```

and finally run chaincode

```bash
root@c3a30b6a5dfd:/opt/gopath/src/chaincode# CORE_CHAINCODE_ID_NAME=mycc:0 ./chaincode_example02
```

WARNING: it's a foreground process, blocks terminal!!!

Now go to the cli container (in the second terminal)

```bash
docker exec -it cli bash
```

install go packages

```bash
go get github.com/op/go-logging
``` 

install and instantiate the chaincode

```bash
peer chaincode install -p chaincodedev/chaincode/ -n mycc -v 0
peer chaincode instantiate -n mycc -v 0 -c '{"Args":["init","a","100","b","200"]}' -C myc
```

Now issue an invoke to move 10 from a to b.

```bash
peer chaincode invoke -n mycc -c '{"Args":["invoke","a","b","10"]}' -C myc
```

Finally, query a. We should see a value of 90.

```bash
peer chaincode query -n mycc -c '{"Args":["query","a"]}' -C myc
```


