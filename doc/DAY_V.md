# DAY V
## Working with users
This time we will be working with users. We will start with getting some more information about the current user, using introduced in Fabric 1.1 (Client Identity Chaincode Library)[https://github.com/hyperledger/fabric/blob/master/core/chaincode/lib/cid/README.md]

Let's start with simple chaincode which prints the information about user (usr_chaincode.go)[../usr_chaincode.go]:
```go
package main

//WARNING - this chaincode's ID is hard-coded in chaincode_example04 to illustrate one way of
import (
    "github.com/hyperledger/fabric/core/chaincode/lib/cid"
    ...
)

 ...
func (t *SimpleChaincode) user_info(stub shim.ChaincodeStubInterface, args []string) pb.Response {
    id, err := cid.GetID(stub)
    mspid, err := cid.GetMSPID(stub)

    attr := "attr1"
    val, ok, err := cid.GetAttributeValue(stub, "attr1")
    if err != nil {
        // There was an error trying to retrieve the attribute
    }
    if !ok {
        logger.Debug("User " + id + "does not have attribute " + attr)
    }
    // Do something with the value of 'val'
    jsonResp := "{\"id\":\"" + id + "\",\"mspid\":\"" + mspid + "\"" + "\",\"attributes\":\"" + val + "\"}"
    logger.Debug(jsonResp)
    return shim.Success(nil)
}

func main() {
    logging.SetBackend(backend1Leveled, backend2Formatter)
    err := shim.Start(new(SimpleChaincode))
    if err != nil {
        logger.Errorf("Error starting Simple chaincode: %s", err)
    }
}
```

In order to compile above code we need to resort to (govendor)[https://github.com/kardianos/govendor] tool to (install dependencies)[https://devcenter.heroku.com/articles/go-dependencies-via-govendor]

```bash
$ docker exec -it chaincode bash
```

```bash
root@339ec465612b:/opt/gopath/src/chaincode/usr#go get -u github.com/kardianos/govendor
```

```bash
root@339ec465612b:/opt/gopath/src/chaincode/usr#govendor init
root@339ec465612b:/opt/gopath/src/chaincode/usr#govendor fetch github.com/hyperledger/fabric/core/chaincode/lib/cid
root@339ec465612b:/opt/gopath/src/chaincode/usr#go build -o usr_chaincode
root@339ec465612b:/opt/gopath/src/chaincode/usr#CORE_PEER_ADDRESS=peer0.org1.example.com:7052 CORE_CHAINCODE_ID_NAME=usr:0 ./usr_chaincode
```

Similar steps needs to be executed in cli container:
```bash
$ docker exec -it cli bash
```

```bash
root@0f2281c1ca82:/opt/gopath/src/usr# go get -u github.com/kardianos/govendor
root@0f2281c1ca82:/opt/gopath/src/usr# govendor init
root@0f2281c1ca82:/opt/gopath/src/usr# govendor fetch github.com/hyperledger/fabric/core/chaincode/lib/cid
root@0f2281c1ca82:/opt/gopath/src/github.com/hyperledger/fabric/peer# peer chaincode install -p github.com/usr -n usr -v 0
root@0f2281c1ca82:/opt/gopath/src/github.com/hyperledger/fabric/peer# peer chaincode instantiate -n usr -v 0 -c '{"Args":[]}' -C mychannel
root@0f2281c1ca82:/opt/gopath/src/github.com/hyperledger/fabric/peer# peer chaincode invoke -n usr -c '{"Args":["user_info"]}' -C mychannel
```

Note that even though we don't use initial arguments `invoke` still requires passing Args, so we need to pass an empty array, like so: `'{"Args":["user_info"]}'`

Since out chaincode only returns success, we need to look at chaincode container to see the output.

```bash
2018-05-17 04:13:09.910 UTC [shim] SetupChaincodeLogging -> INFO 001 Chaincode log level not provided; defaulting to: INFO
2018-05-17 04:13:09.910 UTC [shim] SetupChaincodeLogging -> INFO 002 Chaincode (build level: ) starting up ...
04:19:33.461 Init ▶ DEBU 001 Init Chaincode...
04:20:21.964 Invoke ▶ DEBU 002 Invoking Chaincode...
04:20:33.721 Invoke ▶ DEBU 003 Invoking Chaincode...
04:20:33.724 user_info ▶ DEBU 004 User eDUwOTo6Q049QWRtaW5Ab3JnMS5leGFtcGxlLmNvbSxMPVNhbiBGcmFuY2lzY28sU1Q9Q2FsaWZvcm5pYSxDPVVTOjpDTj1jYS5vcmcxLmV4YW1wbGUuY29tLE89b3JnMS5leGFtcGxlLmNvbSxMPVNhbiBGcmFuY2lzY28sU1Q9Q2FsaWZvcm5pYSxDPVVTdoes not have attribute attr1
04:20:33.724 user_info ▶ DEBU 005 {"id":"eDUwOTo6Q049QWRtaW5Ab3JnMS5leGFtcGxlLmNvbSxMPVNhbiBGcmFuY2lzY28sU1Q9Q2FsaWZvcm5pYSxDPVVTOjpDTj1jYS5vcmcxLmV4YW1wbGUuY29tLE89b3JnMS5leGFtcGxlLmNvbSxMPVNhbiBGcmFuY2lzY28sU1Q9Q2FsaWZvcm5pYSxDPVVT","mspid":"Org1MSP"","attributes":""}
```
