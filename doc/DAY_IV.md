# DAY IV

Today we are going to add [Hyperledger Explorer](https://github.com/hyperledger/blockchain-explorer) to our setup, so that we can observe transactions in real timme.
Let's start with updating our [docker-compose.yml](docker-compose.yml)

```diff
$ git diff docker-compose.yml
diff --git a/docker-compose.yml b/docker-compose.yml
index a9f4833..9246f12 100644
--- a/docker-compose.yml
+++ b/docker-compose.yml
@@ -147,4 +147,33 @@ services:
     networks:
       - basic
     depends_on:
-      - cli
\ No newline at end of file
+      - cli
+
+  explorer:
+    container_name: explorer
+    image: zeepeetek/blockchain-explorer:3.1
+    environment:
+      - POSTGRES_HOST=postgres
+      - POSTGRES_PORT=5432
+      - POSTGRES_USER=postgres
+      - PGPASSWORD=postgres
+      - LOG_LEVEL=debug
+    volumes:
+      - ./config/crypto-config/peerOrganizations/org1.example.com:/etc/hyperledger/msp
+      - ./config/config.json:/var/blockchain-explorer/config.json
+    ports:
+      - 3000:3000
+    depends_on:
+      - postgres
+    networks:
+      - basic
+
+  postgres:
+    container_name: postgres
+    image: postgres:10.3-alpine
+    ports:
+      - 5432:5432
+    environment:
+      - POSTGRES_PASSWORD=postgres
+    networks:
+      - basic
```

There are two new services: blockchain explorer and postgres, the latter being the data storage for explorer.

Explorer service needs to be configured. The configuration is included in the [config/config.json](config/config.json) file.
Important notice: in the devmode we are not using tls, so the peer address should be grpc://, not grpc*s*://


