# DAY I
We will use [hyperledger/fabric-samples/chaincode-docker-devmode/](https://github.com/hyperledger/fabric-samples/tree/master/chaincode-docker-devmode) as a base for the experiments.
First, we need to obtain Hyperledger Fabric docker images. For reasons explained [here](https://jira.hyperledger.org/browse/FAB-8338), hyperledger does not provide latest tag, so one needs to pull specific version. 

Here we will be using 1.0.6 version, since newer versions fails with either *certificate expiration* or *invalid memory address or nil pointer dereference*.

`docker-compose` file from original repository assumes that pulled images are retagged as latest.
Instead of doing it, we will utilize docker-compose environemental variables through `.env` files feature.

First let's rename docker compose file to the default value
```bash
mv docker-compose-simple.yaml docker-compose.yml
```

then add [.env](.env) file to the repository. Env file are simple key=value flat files, which provide environment variables for the docker-compose

```bash
echo TAG=x86_64-1.0.6 > .env
```

and update the docker-compose file refer to that TAG

```yaml
services:
    orderer:
        container_name: orderer
        image: hyperledger/fabric-orderer:$TAG
        environment:
...
```

.env variables can be overriden when invoking docker-compose, like so:

```bash
TAG=1.1.0-pre docker-compose up
```

To test the developer environement we will use *chaincode_example02.go* chaincode from the [fabric/examples/chaincode/go/chaincode_example02](https://github.com/hyperledger/fabric-samples/blob/master/chaincode/chaincode_example02/go/chaincode_example02.go) repository.
Since chaincode is now within the repository, the path to the chaincode volume in the cli and chaincode containers needs to be updated accordingly.

Once everything is ready, we can verify the solution. First we need to spin the docker images
```bash
docker-compose up
```

Shall there be any errors examine them using docker logs feature, like so:

```bash
docker logs peer
docker logs cli
...
```
