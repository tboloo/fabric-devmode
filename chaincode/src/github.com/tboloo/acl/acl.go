package main

import (
	"github.com/op/go-logging"
	"os"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
	"github.com/hyperledger/fabric/core/chaincode/lib/cid"
	"encoding/json"
)

var logger = logging.MustGetLogger("acl")
var format = logging.MustStringFormatter(
	`%{color}%{time:15:04:05.000} %{shortfunc} ▶ %{level:.8s} %{id:03x}%{color:reset} %{message}`,
)

var user_name string

type SimpleChaincode struct {
}

func (t *SimpleChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	logger.Debug("Initializing SimpleChaincode")
	resp := "Initialized"
	logger.Debug(resp)
	return shim.Success([]byte(resp))
}

func (t *SimpleChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	logger.Debug("Invoking Chaincode...")
	val, ok, err := cid.GetAttributeValue(stub, "hf.EnrollmentID")
	if err != nil {
		return shim.Error("Error retriving hf.EnrollmentID, error: " + err.Error())
	}
	if !ok {
		return shim.Success([]byte("User does not have attribute hf.EnrollmentID"))
	}
	user_name = val
	function, args := stub.GetFunctionAndParameters()
	if function == "user_info" {
		return t.user_info(stub, args)
	}
	if function == "get_attribute" {
		return t.get_attribute(stub, args)
	}
	return shim.Error("Invalid invoke function name. Expecting \"user_info\" or \"get_attribute\".")
}

func (t* SimpleChaincode) user_info(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	logger.Debug("Invoking user_info")
	id, _ := cid.GetID(stub)
	mspid, _ := cid.GetMSPID(stub)

	attributes := map[string]string{"attr1":"not set","hf.Affiliation":"not set","hf.EnrollmentID":"not set","hf.Type":"not set"}

	for attrName,_ := range attributes {
		val, ok, err := cid.GetAttributeValue(stub, attrName)
		if err != nil {
			logger.Error("Error retrieving '" + attrName + "' attribute, " + err.Error())
		}
		if ok {
			attributes[attrName] = val
		}
	}
	jsonStr,_ := json.Marshal(attributes)
	jsonResp := "{\"id\":\"" + id + "\",\"mspid\":\"" + mspid + "\"" + ",\"attributes\":" + string(jsonStr) + "}"
	logger.Debug(jsonResp)
	return shim.Success([]byte(jsonResp))
}

func (t* SimpleChaincode) get_attribute(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	var attribute_name string
	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting only attribute name")
	}
	attribute_name = args[0]
	val, ok, err := cid.GetAttributeValue(stub, attribute_name)
	if err != nil {
		return shim.Error("Error retriving attribute " + attribute_name + ", error: " + err.Error())
	}
	if !ok {
		return shim.Error("User " + user_name + " does not have attribute " + attribute_name)
	}
	return shim.Success([]byte(val))
}

func main() {
	backend1 := logging.NewLogBackend(os.Stderr, "", 0)
	backend2 := logging.NewLogBackend(os.Stderr, "", 0)

	// For messages written to backend2 we want to add some additional
	// information to the output, including the used log level and the name of
	// the function.
	backend2Formatter := logging.NewBackendFormatter(backend2, format)

	// Only errors and more severe messages should be sent to backend1
	backend1Leveled := logging.AddModuleLevel(backend1)
	backend1Leveled.SetLevel(logging.ERROR, "")

	logging.SetBackend(backend1Leveled, backend2Formatter)

	logger.Debug("Starting SimpleChaincode")
	err := shim.Start(new(SimpleChaincode))
	if err != nil {
		logger.Errorf("Error starting Simple chaincode: %s", err)
	}
}
