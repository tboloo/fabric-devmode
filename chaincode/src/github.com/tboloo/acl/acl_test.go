package main

import (
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"fmt"
	"testing"
	"regexp"
	"encoding/json"
)

//type mockStub struct {
//	creator []byte
//}
//
//const certificate_with_attributes = `-----BEGIN CERTIFICATE-----
//MIICzjCCAnWgAwIBAgIUUClYpo2aBifrcsChwXr39e2R474wCgYIKoZIzj0EAwIw
//czELMAkGA1UEBhMCVVMxEzARBgNVBAgTCkNhbGlmb3JuaWExFjAUBgNVBAcTDVNh
//biBGcmFuY2lzY28xGTAXBgNVBAoTEG9yZzEuZXhhbXBsZS5jb20xHDAaBgNVBAMT
//E2NhLm9yZzEuZXhhbXBsZS5jb20wHhcNMTgwNTI3MTQ1MjAwWhcNMTkwNTI3MTQ1
//NzAwWjBoMQswCQYDVQQGEwJVUzEXMBUGA1UECBMOTm9ydGggQ2Fyb2xpbmExFDAS
//BgNVBAoTC0h5cGVybGVkZ2VyMRowCwYDVQQLEwR1c2VyMAsGA1UECxMEb3JnMTEO
//MAwGA1UEAxMFdXNlcjEwWTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAAT7vkIJvw2j
//LqZeGqbnh4pR1z8cFTrQGj2w7uCRiXXxiph73GZyyC4cFYTWVRiosJ5fZhaUUOh7
//l0RGqLRc305jo4HxMIHuMA4GA1UdDwEB/wQEAwIHgDAMBgNVHRMBAf8EAjAAMB0G
//A1UdDgQWBBQSe1V88m4eh8HLDUY/HMCmQRl36TArBgNVHSMEJDAigCAt/H+w5FSt
//jmfCzh2/QfR7brrgLQb/ppdAMiY9s4TVVDAXBgNVHREEEDAOggwyNzY4YWYzMGE4
//ZTIwaQYIKgMEBQYHCAEEXXsiYXR0cnMiOnsiYXR0cjEiOiJ2YWwxIiwiaGYuQWZm
//aWxpYXRpb24iOiJvcmcxIiwiaGYuRW5yb2xsbWVudElEIjoidXNlcjEiLCJoZi5U
//eXBlIjoidXNlciJ9fTAKBggqhkjOPQQDAgNHADBEAiA8susAi1sH5sVPB+EL4QD9
//8+GnKVXXyoNM+IImwK7+9gIgOLFJun/k9Uv49VonpBhvERdzLQvPg9hcdqh4eFCd
//gX4=
//-----END CERTIFICATE-----
//`

//func getMockStub() (cid.ChaincodeStubInterface, error) {
//	stub := &mockStub{}
//	sid := &msp.SerializedIdentity{Mspid: "DEFAULT",
//		IdBytes: []byte(certificate_with_attributes)}
//	b, err := proto.Marshal(sid)
//	if err != nil {
//		return nil, err
//	}
//	stub.creator = b
//	return stub, nil
//}

func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("checkInit failed", string(res.Message))
		t.FailNow()
	}
	msg := string(res.Payload[:])
	if msg != "Initialized" {
		fmt.Println("checkInit failed", string(res.Message))
		t.FailNow()
	}
}

func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
	res := stub.MockInvoke("1", args)
	msg := string(res.Payload[:])
	matched, _ := regexp.MatchString("id.*mspid.*attributes.*\\[", msg)
	if matched == false {
		fmt.Println("Expected pattern 'id.*mspid.*attributes.*\\[' to match the result of Invoke")
		t.FailNow()
	}

	var result map[string]interface{}

	if err := json.Unmarshal(res.Payload, &result); err != nil {
		panic(err)
	}

	_, ok := result["id"]
	if !ok {
		fmt.Println("Expected result to have 'id' key")
		t.FailNow()
	}

	_, ok = result["mspid"]
	if !ok {
		fmt.Println("Expected result to have 'mspid' key")
		t.FailNow()
	}

	_, ok = result["attributes"]
	if !ok {
		fmt.Println("Expected result to have 'attributes' key")
		t.FailNow()
	}
	if res.Status != shim.OK {
		fmt.Println("Invoke", args, "failed", string(res.Message))
		t.FailNow()
	}
}

func TestAcl_Init(t *testing.T) {
	scc := new(SimpleChaincode)
	stub := shim.NewMockStub("acl", scc)

	checkInit(t, stub, [][]byte{[]byte("")})
}

func TestAcl_Invoke(t *testing.T) {
	scc := new(SimpleChaincode)
	stub := shim.NewMockStub("acl", scc)
	checkInvoke(t,stub, [][]byte{[]byte("user_info"), []byte("")})
}