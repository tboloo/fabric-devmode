var Fabric_Client = require('fabric-client');
var Fabric_CA_Client = require('fabric-ca-client');
var path = require('path');
var util = require('util');
var os = require('os');

const fabric_client = new Fabric_Client();

  // setup the fabric network
  const channel = fabric_client.newChannel('mychannel');
  const peer = fabric_client.newPeer('grpc://localhost:7051');
  channel.addPeer(peer);

  const store_path = path.join(__dirname, 'hfc-key-store');
  const tx_id = null;
  // let member_user = null;
  async function getAllIdentities() {
    try {
      const stateStore = await Fabric_Client.newDefaultKeyValueStore({ path: store_path });
      fabric_client.setStateStore(stateStore);
      const cryptoSuite = Fabric_Client.newCryptoSuite();
      const cryptoStore = Fabric_Client.newCryptoKeyStore({ path: store_path });
      cryptoSuite.setCryptoKeyStore(cryptoStore);
      fabric_client.setCryptoSuite(cryptoSuite);

      const fabricCAClient = new Fabric_CA_Client('http://ca.org1.example.com:7054', null , '', cryptoSuite);
      const adminFromStore = await fabric_client.getUserContext('admin', true);
      if (adminFromStore && adminFromStore.isEnrolled()) {
        console.log('Successfully loaded admin from persistence');
      } else {
        throw new Error('Failed to get admin');
      }
      const identityService = fabricCAClient.newIdentityService('admin');
      let identities = await identityService.getAll(adminFromStore);
      console.log(JSON.stringify(identities));
    } catch (err) {
      console.error('queryAll err:', err);
    }
  }

  getAllIdentities();

