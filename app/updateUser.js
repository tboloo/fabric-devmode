var Fabric_Client = require('fabric-client');
var Fabric_CA_Client = require('fabric-ca-client');

var path = require('path');
var util = require('util');
var os = require('os');

//
var fabric_client = new Fabric_Client();
var attribute = ''
var user = ''
var attribute = ''
if ((process.argv).length > 4) {
	user = process.argv[2]
	attribute = process.argv[3]
	value = process.argv[4]
} else {
	console.log("Usage: node " + process.argv[1] + " username attribute value")
	process.exit(1)
}
// setup the fabric network
var channel = fabric_client.newChannel('mychannel');
var peer = fabric_client.newPeer('grpc://peer0.org1.example.com:7051');
channel.addPeer(peer);

//
var member_user = null;
var store_path = path.join(__dirname, 'hfc-key-store');
console.log('Store path:'+store_path);
var tx_id = null;

async function updateUser(user,attribute,value) {
	try {
      const stateStore = await Fabric_Client.newDefaultKeyValueStore({ path: store_path });
      fabric_client.setStateStore(stateStore);
      const cryptoSuite = Fabric_Client.newCryptoSuite();
      const cryptoStore = Fabric_Client.newCryptoKeyStore({ path: store_path });
      cryptoSuite.setCryptoKeyStore(cryptoStore);
      fabric_client.setCryptoSuite(cryptoSuite);

      const fabricCAClient = new Fabric_CA_Client('http://ca.org1.example.com:7054', null , '', cryptoSuite);
      const adminFromStore = await fabric_client.getUserContext('admin', true);
      if (adminFromStore && adminFromStore.isEnrolled()) {
        console.log('Successfully loaded admin from persistence');
      } else {
        throw new Error('Failed to get admin');
      }
      const identityService = fabricCAClient.newIdentityService('admin');

      var request = {enrollmentID:user,affiliation:'org1',attrs:[]}
      attr1 = {name:attribute,value:value,ecert:true}
      request.attrs.push(attr1)
      let response = await identityService.update(user,request,adminFromStore);
      console.log(JSON.stringify(response));

    } catch (err) {
      console.error('updateUser err:', err);
    }
}

updateUser(user,attribute,value);