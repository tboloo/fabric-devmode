var express       = require('express');        // call express
var app           = express();                 // define our app using express
var bodyParser    = require('body-parser');

var port = process.env.PORT || 4000;

// Start the server and listen on port 
app.listen(port,function(){
  console.log("Live on port: " + port);
});