#!/bin/bash

usage() {
    echo "Usage: $0
    [-n|--name <chaincode name>]
    [-v|--version <chaincode version>
    [-i|--intantiate_arguments <chaincode instantiate agruments>]
    [-c|--channel <channel name>]
    " 1>&2;
    exit 0;
}

CHAINCODE_NAME=chaincode
CHAINCODE_VERSION=0
INSTATIATE_ARGUMENTS='{"Args":[]}'
CHANNEL_NAME=mychannel

while (( "$#" )); do
  case "$1" in
    -n | --name)
      CHAINCODE_NAME=$2
      shift 2
      ;;
    -v | --version)
      CHAINCODE_VERSION=$2
      shift 2
      ;;
    -i | --intantiate_arguments)
      INSTATIATE_ARGUMENTS=$2
      shift 2
      ;;
    -c | --channel)
      CHANNEL_NAME=$2
      shift 2
      ;;
    -h | --help )
      usage
      break;;
    --) # end argument parsing
      shift
      break
      ;;
    -*|--*=) # unsupported flags
      echo "Unsupported option $1"
      usage
      exit 1
      ;;
  esac
done
# set positional arguments in their proper place
eval set -- "$PARAMS"

echo "Upgrading chaincode $CHAINCODE_NAME to version $CHAINCODE_VERSION on channel $CHANNEL_NAME"

echo "instantiating chaincode"
docker exec --interactive --tty --workdir=/opt/gopath/src/github.com/hyperledger/fabric/peer cli peer chaincode upgrade -n ${CHAINCODE_NAME} -v ${CHAINCODE_VERSION} -c ${INSTATIATE_ARGUMENTS} -C ${CHANNEL_NAME}