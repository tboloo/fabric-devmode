#!/bin/sh
#
# Copyright IBM Corp All Rights Reserved
#
# SPDX-License-Identifier: Apache-2.0
#
set -e

usage() {
    echo "Usage: $0
    [-b|--bin_path <hyperledger binaries path>]
    [-c|--config_path <config path>]
    [-a|--artifacts <artifacts path> 
    [--channel_name <channel name>
    [-o|--org_name <organisation name>
    [--output <output path>] 
    [--keyfile_path <path to admin key file>]
    " 1>&2;
    exit 0;
}

BIN_PATH=${PWD}/bin
CONFIG_PATH=${PWD}/config
FABRIC_CFG_PATH=${CONFIG_PATH}
ARTIFACTS_PATH=${PWD}/artifacts
CHANNEL_NAME=mychannel
ORG_NAME=Org1MSP
OUTPUT_PATH=${CONFIG_PATH}/crypto-config
KEYFILE_PATH=${OUTPUT_PATH}/peerOrganizations/org1.example.com/ca/

while (( "$#" )); do
  case "$1" in
    -b | --bin_path)
      BIN_PATH=$2
      shift 2
      ;;
    -c | --config_path)
      CONFIG_PATH=$2
      shift 2
      ;;
    -a | --artifacts)
      ARTIFACTS_PATH=$2
      shift 2
      ;;
    --channel_name)
      CHANNEL_NAME=$2
      shift 2
      ;;
    -o | --org_name)
      ORG_NAME=$2
      shift 2
      ;;
    --output)
      OUTPUT_PATH=$2
      shift 2
      ;;
    -h | --help )
      usage
      break;;
    --) # end argument parsing
      shift
      break
      ;;
    -*|--*=) # unsupported flags
      echo "Unsupported option $1"
      usage
      exit 1
      ;;
  esac
done

# remove previous crypto material and config transactions
# rm -fr config/*
echo "cleaning previous config"
rm -rf ${OUTPUT_PATH}/*

PATH=$BIN_PATH:$PATH

echo "Generating crypto material for $ORG_NAME, channel $CHANNEL_NAME"
# generate crypto material
cryptogen generate --config=${CONFIG_PATH}/crypto-config.yaml --output=${OUTPUT_PATH}
if [ "$?" -ne 0 ]; then
  echo "Failed to generate crypto material..."
  exit 1
fi

# print the organisation configuration
FABRIC_CFG_PATH=${FABRIC_CFG_PATH} configtxgen -printOrg $ORG_NAME
if [ "$?" -ne 0 ]; then
  echo "Failed to print organisation ${ORG_NAME} definition ..."
  exit 1
fi

# generate genesis block for orderer
FABRIC_CFG_PATH=${FABRIC_CFG_PATH} configtxgen -profile OneOrgOrdererGenesis -outputBlock ${ARTIFACTS_PATH}/genesis.block
if [ "$?" -ne 0 ]; then
  echo "Failed to generate orderer genesis block..."
  exit 1
fi

# generate channel configuration transaction
FABRIC_CFG_PATH=${FABRIC_CFG_PATH} configtxgen -profile OneOrgChannel -outputCreateChannelTx ${ARTIFACTS_PATH}/channel.tx -channelID $CHANNEL_NAME
if [ "$?" -ne 0 ]; then
  echo "Failed to generate channel configuration transaction..."
  exit 1
fi

# generate anchor peer transaction
FABRIC_CFG_PATH=${FABRIC_CFG_PATH} configtxgen -profile OneOrgChannel -outputAnchorPeersUpdate ${ARTIFACTS_PATH}/${ORG_NAME}anchors.tx -channelID $CHANNEL_NAME -asOrg ${ORG_NAME}
if [ "$?" -ne 0 ]; then
  echo "Failed to generate anchor peer update for ${ORG_NAME}..."
  exit 1
fi

FABRIC_CA_SERVER_CA_KEYFILE=`ls $KEYFILE_PATH | grep _sk`

echo "updating FABRIC_CA_SERVER_CA_KEYFILE config"

sed -i -E 's/[a-z0-9]+_sk/'"$FABRIC_CA_SERVER_CA_KEYFILE"'/g' .env