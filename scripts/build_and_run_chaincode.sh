#!/bin/bash
# set -ev

usage() {
    echo "Usage: $0
    [-p|--package <package name>]
    [-n|--name <chaincode name>]
    [-v|--version <chaincode version> 
    " 1>&2;
    exit 0;
}


PACKAGE=chaincode/pkg
CHAINCODE_NAME=chaincode
CHAINCODE_VERSION=0

while (( "$#" )); do
  case "$1" in
    -p | --package)
      PACKAGE=$2
      shift 2
      ;;
    -n | --name)
      CHAINCODE_NAME=$2
      shift 2
      ;;
    -v | --version)
      CHAINCODE_VERSION=$2
      shift 2
      ;;
    -h | --help )
      usage
      break;;
    --) # end argument parsing
      shift
      break
      ;;
    -*|--*=) # unsupported flags
      echo "Unsupported option $1"
      usage
      exit 1
      ;;
  esac
done
# set positional arguments in their proper place
eval set -- "$PARAMS"

IFS='/' read -r -a array <<< "$PACKAGE"
BINARY=${array[-1]}

echo "building chaincode $CHAINCODE_NAME from $PACKAGE in version $CHAINCODE_VERSION"


echo "cleaning up"
docker exec --interactive --tty --workdir=/opt/gopath/ chaincode rm -rf src/chaincode
docker exec --interactive --tty --workdir=/opt/gopath/ chaincode rm -rf src/github.com/hyperledger
docker exec --interactive --tty --workdir=/opt/gopath/ chaincode rm -rf bin/${BINARY}
echo "getting dependencies"
docker exec --interactive --tty --workdir=/opt/gopath/ chaincode go get -v -u github.com/op/go-logging
docker exec --interactive --tty --workdir=/opt/gopath/ chaincode go get -v -u github.com/hyperledger/fabric/bccsp
docker exec --interactive --tty --workdir=/opt/gopath/ chaincode go get -v -u github.com/hyperledger/fabric/core
docker exec --interactive --tty --workdir=/opt/gopath/ chaincode go get -v -u github.com/hyperledger/fabric/idemix
docker exec --interactive --tty --workdir=/opt/gopath/ chaincode go get -v -u github.com/hyperledger/fabric/msp
echo "building chaincode"
docker exec --interactive --tty --workdir=/opt/gopath/ chaincode go install -v ${PACKAGE}
echo "starting chaincode"
docker exec --interactive --tty --workdir=/opt/gopath/bin -e CORE_CHAINCODE_ID_NAME=${CHAINCODE_NAME}:${CHAINCODE_VERSION} chaincode ./${BINARY}
