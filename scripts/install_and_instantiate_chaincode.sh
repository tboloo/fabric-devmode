#!/bin/bash

usage() {
    echo "Usage: $0
    [-p|--package <package name>]
    [-n|--name <chaincode name>]
    [-v|--version <chaincode version>
    [-i|--intantiate_arguments <chaincode instantiate agruments>]
    [-c|--channel <channel name>]
    " 1>&2;
    exit 0;
}

PACKAGE="chaincode/pkg"
BINARY=chaincode
CHAINCODE_NAME=chaincode
CHAINCODE_VERSION=0
INSTATIATE_ARGUMENTS='{"Args":[]}'
CHANNEL_NAME=mychannel

while (( "$#" )); do
  case "$1" in
    -p | --package)
      PACKAGE=$2
      shift 2
      ;;
    -n | --name)
      CHAINCODE_NAME=$2
      shift 2
      ;;
    -v | --version)
      CHAINCODE_VERSION=$2
      shift 2
      ;;
    -i | --intantiate_arguments)
      INSTATIATE_ARGUMENTS=$2
      shift 2
      ;;
    -c | --channel)
      CHANNEL_NAME=$2
      shift 2
      ;;
    -h | --help )
      usage
      break;;
    --) # end argument parsing
      shift
      break
      ;;
    -*|--*=) # unsupported flags
      echo "Unsupported option $1"
      usage
      exit 1
      ;;
  esac
done
# set positional arguments in their proper place
eval set -- "$PARAMS"

echo "installing chaincode $CHAINCODE_NAME from $PACKAGE in version $CHAINCODE_VERSION on channel $CHANNEL_NAME"

echo "getting dependencies"
docker exec --interactive --tty --workdir=/opt/gopath/ chaincode go get -v -u github.com/op/go-logging
echo "installing chaincode"
docker exec --interactive --tty --workdir=/opt/gopath/src/github.com/hyperledger/fabric/peer cli peer chaincode install -p ${PACKAGE} -n ${CHAINCODE_NAME} -v ${CHAINCODE_VERSION}
echo "instantiating chaincode"
docker exec --interactive --tty --workdir=/opt/gopath/src/github.com/hyperledger/fabric/peer cli peer chaincode instantiate -n ${CHAINCODE_NAME} -v ${CHAINCODE_VERSION} -c ${INSTATIATE_ARGUMENTS} -C ${CHANNEL_NAME}
#echo "invoking chaincode"
#docker exec --interactive --tty --workdir=/opt/gopath/src/github.com/hyperledger/fabric/peer cli peer chaincode invoke -n usr -c '{"Args":["user_info"]}' -C mychannel
