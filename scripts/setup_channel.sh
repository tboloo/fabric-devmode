#!/bin/bash

usage() {
    echo "Usage: $0
    [-c|--channel <channel name>]
    " 1>&2;
    exit 0;
}

CHANNEL=mychannel
# don't rewrite paths for Windows Git Bash users
export MSYS_NO_PATHCONV=1


while (( "$#" )); do
  case "$1" in
    -c | --channel)
      CHANNEL=$2
      shift 2
      ;;
    -h | --help )
      usage
      break;;
    --) # end argument parsing
      shift
      break
      ;;
    -*|--*=) # unsupported flags
      echo "Unsupported option $1"
      usage
      exit 1
      ;;
  esac
done

echo "Creating and joining channel $CHANNEL"
# Create the channel
docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.example.com/msp" peer0.org1.example.com peer channel create -o orderer.example.com:7050 -c ${CHANNEL} -f /etc/hyperledger/configtx/channel.tx
# Join peer0.org1.example.com to the channel.
docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.example.com/msp" peer0.org1.example.com peer channel join -b ${CHANNEL}.block